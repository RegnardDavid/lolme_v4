package com.eilco.davidregnard.lolme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.eilco.davidregnard.lolme.RetrofitService.APIKey;
import static com.eilco.davidregnard.lolme.RetrofitService.ENDPOINT;

public class MainActivity extends AppCompatActivity {

    private EditText summonerName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        summonerName = findViewById(R.id.UserName);
        Button button = findViewById(R.id.get);
        Log.d("etape 1","bouton presse summonerName : "+ summonerName.getText().toString());


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("etape 1","bouton presse summonerName : "+ summonerName.getText().toString());

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ENDPOINT)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                retrofit.create(RetrofitService.class);
                final RetrofitService service = retrofit.create(RetrofitService.class);
                service.getSummoner(summonerName.getText().toString(),APIKey).enqueue(new Callback<Summoner>() {
                    @Override
                    public void onResponse(Call<Summoner> call, Response<Summoner> response) {
                        final Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                        Summoner summoner = response.body();
                        intent.putExtra("summoner",summoner);

                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<Summoner> call, Throwable t) {
                        Log.d("error","test");
                    }
                });

            }
        });
    }


}
