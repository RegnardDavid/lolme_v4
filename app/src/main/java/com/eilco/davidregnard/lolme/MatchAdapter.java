package com.eilco.davidregnard.lolme;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.ViewHolder>{

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView lane;
        public TextView role;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.tv_item_match);
            role = (TextView) itemView.findViewById(R.id.role_item);
            lane = (TextView) itemView.findViewById(R.id.lane_item);
        }
    }

    private List<MatchReferenceDto> Matchs;
    private Context context;

    public MatchAdapter(Context context,List<MatchReferenceDto> c) {
        this.context = context;
        Matchs = c;
    }

    public void setMatch(List<MatchReferenceDto> matchList) {
        this.Matchs = matchList;
        notifyDataSetChanged();
    }

    @Override
    public MatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View matchView = inflater.inflate(R.layout.match_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(matchView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MatchAdapter.ViewHolder viewHolder, int position) {

        MatchReferenceDto match = Matchs.get(position);

        TextView textView = viewHolder.nameTextView;
        textView.setText(Long.toString(match.getGameId()));
        viewHolder.lane.setText(match.getLane());
        viewHolder.role.setText(match.getRole());
    }

    @Override
    public int getItemCount() {
        if(Matchs!= null){
            return Matchs.size();
        }
        return 0;
    }
}