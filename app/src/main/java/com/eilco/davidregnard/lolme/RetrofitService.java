package com.eilco.davidregnard.lolme;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    public static final String ENDPOINT = "https://euw1.api.riotgames.com";

    public static final String APIKey = "RGAPI-bd6c036f-3622-4366-b5cf-1c78d90e375d";

    @GET("/lol/summoner/v4/summoners/by-name/{summonerName}")
    Call<Summoner> getSummoner(@Path("summonerName") String summonerName, @Query("api_key") String ApiKey);

    @GET("/lol/league/v4/positions/by-summoner/{summonerId}")
    Call<ArrayList<Position>> getPositionsBySummonerID(@Path("summonerId") String summonerId, @Query("api_key") String ApiKey);

    @GET("/lol/match/v4/matchlists/by-account/{accountId}")
    Call<MatchlistDto> getMatchesByAccountID(@Path("accountId") String summonerId, @Query("api_key") String ApiKey);
}